package ro.bogdan.energytool.sensor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.bogdan.energytool.sensor.model.MonitoredData;

@Repository
public interface MonitoredDataRepo extends JpaRepository<MonitoredData,Long> {
}
