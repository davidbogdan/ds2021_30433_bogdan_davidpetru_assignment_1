package ro.bogdan.energytool.sensor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.bogdan.energytool.sensor.model.Sensor;

import java.util.Optional;

@Repository
public interface SensorRepo extends JpaRepository<Sensor,Long> {
    Optional<Sensor> findByUid(String uid);
}
