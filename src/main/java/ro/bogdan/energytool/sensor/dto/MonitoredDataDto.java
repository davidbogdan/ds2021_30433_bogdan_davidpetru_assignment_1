package ro.bogdan.energytool.sensor.dto;

import lombok.*;
import ro.bogdan.energytool.sensor.model.Sensor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MonitoredDataDto {
    private Date date;
    private Integer energyConsumption;
    private String sensorUid;
}
