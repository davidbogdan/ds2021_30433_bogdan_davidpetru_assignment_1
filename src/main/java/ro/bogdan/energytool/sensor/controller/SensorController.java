package ro.bogdan.energytool.sensor.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.bogdan.energytool.sensor.dto.MonitoredDataDto;
import ro.bogdan.energytool.sensor.dto.SensorDto;
import ro.bogdan.energytool.sensor.model.MonitoredData;
import ro.bogdan.energytool.sensor.service.SensorService;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/sensors")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SensorController {
    private final SensorService sensorService;

    @GetMapping("")
    public ResponseEntity<List<SensorDto>> findAll(){
        return ResponseEntity.ok(sensorService.findAll());
    }
    @PostMapping("")
    public ResponseEntity<SensorDto> create(@RequestBody SensorDto sensorDto){
        return new ResponseEntity<>(sensorService.save(sensorDto), HttpStatus.CREATED);
    }
    @GetMapping("/{sensorId}")
    public ResponseEntity<SensorDto> findByUid(@PathVariable String sensorId){
        return new ResponseEntity<>(sensorService.findByUid(sensorId),HttpStatus.OK);
    }
    @PutMapping("/{sensorId}")
    public ResponseEntity<SensorDto> update(@RequestBody SensorDto sensorDto, @PathVariable String sensorId){
        return new ResponseEntity<>(sensorService.update(sensorDto,sensorId),HttpStatus.OK);
    }
    @DeleteMapping("/{sensorId}")
    public ResponseEntity<?> delete(@PathVariable String sensorId){
        sensorService.delete(sensorId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @GetMapping("/{sensorId}/monitored-data")
    public ResponseEntity<Set<MonitoredDataDto>> getAllMonitoredData(@PathVariable String sensorId){
        return ResponseEntity.ok(sensorService.getAllMonitoredData(sensorId));
    }
    @PostMapping("/{sensorId}/monitored-data")
    public ResponseEntity<MonitoredDataDto> saveMonitoredData(@PathVariable String sensorId, @RequestBody MonitoredDataDto monitoredData){
        return ResponseEntity.ok(sensorService.saveMonitoredData(sensorId,monitoredData));
    }
}
