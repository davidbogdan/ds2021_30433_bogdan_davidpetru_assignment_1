package ro.bogdan.energytool.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import ro.bogdan.energytool.device.repository.DeviceRepo;
import ro.bogdan.energytool.queue.MessageReceiver;
import ro.bogdan.energytool.sensor.repository.MonitoredDataRepo;
import ro.bogdan.energytool.sensor.repository.SensorRepo;
import ro.bogdan.energytool.user.repository.UserRepo;

//@Profile({"config","hello-world"})
@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class QueueConfig {
    private final SensorRepo sensorRepo;
    private final MonitoredDataRepo monitoredDataRepo;
    private final SimpMessagingTemplate template;
    @Bean
    public MessageReceiver receiver() {
        return new MessageReceiver(sensorRepo,monitoredDataRepo,template);
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("roedeer.rmq.cloudamqp.com");
        connectionFactory.setUsername("vpjsueax");
        connectionFactory.setVirtualHost("vpjsueax");
        connectionFactory.setPassword("4v80hSF5D8lzpfMXuOwZvy5EtEQ1FuZO");
        return connectionFactory;
    }
}
