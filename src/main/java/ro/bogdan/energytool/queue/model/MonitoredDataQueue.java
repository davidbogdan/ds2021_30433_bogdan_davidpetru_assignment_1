package ro.bogdan.energytool.queue.model;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
public class MonitoredDataQueue {
    private long timestamp;
    private Integer energyConsumption;
    private String sensorUid;
}
