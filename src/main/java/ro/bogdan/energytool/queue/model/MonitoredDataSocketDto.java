package ro.bogdan.energytool.queue.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@ToString
public class MonitoredDataSocketDto {
    private String ownerUid;
    private String message;
}
