package ro.bogdan.energytool.queue;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.realm.UserDatabaseRealm;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import ro.bogdan.energytool.device.exception.DeviceNotFoundException;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.device.repository.DeviceRepo;
import ro.bogdan.energytool.queue.model.MonitoredDataQueue;
import ro.bogdan.energytool.queue.model.MonitoredDataSocketDto;
import ro.bogdan.energytool.sensor.exception.SensorNotFoundException;
import ro.bogdan.energytool.sensor.model.MonitoredData;
import ro.bogdan.energytool.sensor.model.Sensor;
import ro.bogdan.energytool.sensor.repository.MonitoredDataRepo;
import ro.bogdan.energytool.sensor.repository.SensorRepo;
import ro.bogdan.energytool.user.repository.UserRepo;

import java.util.Date;

@RabbitListener(queues={"sensor1","sensor2","sensor3"})
@Controller
@RequiredArgsConstructor
public class MessageReceiver {
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final SensorRepo sensorRepo;
    private final MonitoredDataRepo monitoredDataRepo;
    private final SimpMessagingTemplate template;
//    private Integer previousValue;
//    private long previousTime;

    @RabbitHandler
    public void receive(String in) throws Exception {
        MonitoredDataQueue monitoredDataQueue = objectMapper.readValue(in,MonitoredDataQueue.class);
        System.out.println(" [x] Received '" + monitoredDataQueue.toString() + "'");
        Sensor sensor = sensorRepo.findByUid(monitoredDataQueue.getSensorUid()).orElseThrow(()-> new SensorNotFoundException(monitoredDataQueue.getSensorUid()));
        MonitoredData monitoredData = MonitoredData.builder().date(new Date(monitoredDataQueue.getTimestamp()))
                                                    .energyConsumption(monitoredDataQueue.getEnergyConsumption())
                                                    .sensor(sensor)
                                                    .build();
//        if(previousValue!=null){
//            Integer valueDifference = monitoredDataQueue.getEnergyConsumption()-previousValue;
//            Integer timeDifference = Math.toIntExact(monitoredDataQueue.getTimestamp() - previousTime);
//            System.out.println("Value diffenrece: " + valueDifference + " time difference: " + timeDifference);
//            if(valueDifference/timeDifference>5){
//                Device device = sensor.getDevice();
//                greeting(device.getOwner().getUid());
//            }
        if(monitoredData.getEnergyConsumption()>sensor.getMax_value()){
                Device device = sensor.getDevice();
                greeting(device.getOwner().getUid());
        }
        monitoredDataRepo.save(monitoredData);
//        previousValue=monitoredDataQueue.getEnergyConsumption();
//        previousTime= monitoredDataQueue.getTimestamp();
    }

    @SendTo("/topic/greetings")
    public void greeting(String ownerUid) throws Exception {
        System.out.println("SENT MESSAGE");
        MonitoredDataSocketDto monitoredDataSocketDto = MonitoredDataSocketDto.builder().message("The maximum value of the sensor was exceeded").ownerUid(ownerUid).build();
        this.template.convertAndSend("/topic/greetings", objectMapper.writeValueAsString(monitoredDataSocketDto));
    }

}
