package ro.bogdan.energytool.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.user.model.Role;
import ro.bogdan.energytool.user.model.User;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,Long> {
    Optional<User> findByUid(String uid);
    Optional<User> findByUsername(String username);
    Optional<User> findByRole(Role role);
//    Optional<User> findByDevice(Device device);
}
