package ro.bogdan.energytool.user.model;

import lombok.*;
import ro.bogdan.energytool.device.model.Device;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "persons")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String uid;
    private String name;
    @Column(unique = true)
    private String username;
    private String password;
    @Column(name="birth_date")
    private Date birthdate;
    private String address;
    private Role role;
    @OneToMany(fetch = FetchType.EAGER,mappedBy = "owner",orphanRemoval = true)
    private Set<Device> deviceSet;


    public void removeDevice(Device device){
        deviceSet.remove(device);
    }
}
