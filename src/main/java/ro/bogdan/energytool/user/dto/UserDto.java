package ro.bogdan.energytool.user.dto;

import lombok.*;

import java.util.Date;
import java.util.Set;
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserDto {
    private String uid;
    private String name;
    private Date birthdate;
    private String address;
    private String username;
    private String password;
   // private Role role;
    private Set<String> deviceUidSet;
}
