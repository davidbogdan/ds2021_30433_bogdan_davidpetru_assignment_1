package ro.bogdan.energytool.user.controller;

import jdk.net.SocketFlow;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.bogdan.energytool.user.dto.UserDto;
import ro.bogdan.energytool.user.service.UserService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/users")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    @GetMapping("")
    public ResponseEntity<List<UserDto>> findAll(){
        return ResponseEntity.ok(userService.findAll());
    }
    @PostMapping("")
    public ResponseEntity<UserDto> create(@RequestBody UserDto userDto){
        return new ResponseEntity<>(userService.save(userDto), HttpStatus.CREATED);
    }
    @GetMapping("/{userId}")
    public ResponseEntity<UserDto> findByUid(@PathVariable String userId){
        return new ResponseEntity<>(userService.findByUid(userId),HttpStatus.OK);
    }
    @PutMapping("/{userId}")
    public ResponseEntity<UserDto> update(@RequestBody UserDto userDto, @PathVariable String userId){
        return new ResponseEntity<>(userService.update(userDto,userId),HttpStatus.OK);
    }
    @DeleteMapping("/{userId}")
    public ResponseEntity<?> delete(@PathVariable String userId){
        userService.delete(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
