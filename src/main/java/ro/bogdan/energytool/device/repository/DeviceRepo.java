package ro.bogdan.energytool.device.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.sensor.model.Sensor;
import ro.bogdan.energytool.user.model.User;

import java.util.Optional;

@Repository
public interface DeviceRepo extends JpaRepository<Device,Long> {
    Optional<Device> findByUid(String uid);
//    Optional<Device> findBySensor(Sensor sensor);
}
