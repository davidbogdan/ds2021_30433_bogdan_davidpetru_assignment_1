package ro.bogdan.energytool.device.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.bogdan.energytool.device.dto.DeviceDto;
import ro.bogdan.energytool.device.exception.DeviceNotFoundException;
import ro.bogdan.energytool.device.model.Device;
import ro.bogdan.energytool.device.repository.DeviceRepo;
import ro.bogdan.energytool.sensor.exception.SensorNotFoundException;
import ro.bogdan.energytool.sensor.model.Sensor;
import ro.bogdan.energytool.sensor.repository.SensorRepo;
import ro.bogdan.energytool.user.exception.UserNotFoundException;
import ro.bogdan.energytool.user.model.User;
import ro.bogdan.energytool.user.repository.UserRepo;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DeviceService {
    private final UserRepo userRepo;
    private final DeviceRepo deviceRepo;
    private final SensorRepo sensorRepo;
    private final ModelMapper modelMapper;

    public DeviceDto save(DeviceDto deviceDto){
        deviceDto.setUid(UUID.randomUUID().toString());
        Device device = fromDtoToEntity(deviceDto);
       // device.setSensor(null);
        deviceRepo.save(device);
        return fromEntityToDto(device);//fromEntityToDto(updateSensorAndDevice(deviceDto,device));
    }

    public List<DeviceDto> findAll(){
        return deviceRepo.findAll().stream().map(this::fromEntityToDto).collect(Collectors.toList());
    }

    public DeviceDto findByUid(String uid){
        return fromEntityToDto(deviceRepo.findByUid(uid).orElseThrow(()->new DeviceNotFoundException(uid)));
    }

    public DeviceDto update(DeviceDto deviceDto, String uid){
        Device oldDevice = deviceRepo.findByUid(uid).orElseThrow(()->new DeviceNotFoundException(uid));
        return fromEntityToDto(deviceRepo.save(updateUserFromDto(oldDevice,deviceDto)));
    }

    public void delete(String uid){
        Device device = deviceRepo.findByUid(uid).orElseThrow(()->new DeviceNotFoundException(uid));
        User owner = device.getOwner();
        owner.removeDevice(device);
        deviceRepo.delete(device);
    }

    private Device updateSensorAndDevice(DeviceDto deviceDto,Device device){
        if(deviceDto.getSensorUid()!=null){
            Sensor sensor = sensorRepo.findByUid(deviceDto.getSensorUid()).orElseThrow(()->new SensorNotFoundException(deviceDto.getSensorUid()));
            device.setSensor(sensor);
            Device result = deviceRepo.save(device);
            sensor.setDevice(device);
            sensorRepo.save(sensor);
            return result;
        }
        return device;
    }

    private Device updateUserFromDto(Device device, DeviceDto deviceDto){
        if(deviceDto.getSensorUid()!=null){
            device.setSensor(sensorRepo.findByUid(deviceDto.getSensorUid()).orElseThrow(()-> new SensorNotFoundException(deviceDto.getSensorUid())));
        }
        if(deviceDto.getDescription()!=null){
            device.setDescription(deviceDto.getDescription());
        }
        if(deviceDto.getLocation()!=null){
            device.setLocation(deviceDto.getLocation());
        }
        if(deviceDto.getAverageConsumption()!=null){
            device.setAverageConsumption(deviceDto.getAverageConsumption());
        }
        if(deviceDto.getMaxConsumption()!=null){
            device.setMaxConsumption(deviceDto.getMaxConsumption());
        }
        if(deviceDto.getOwnerUid()!=null){
            device.setOwner(userRepo.findByUid(deviceDto.getOwnerUid()).orElseThrow(()-> new UserNotFoundException(deviceDto.getOwnerUid())));
        }
        return device;
    }
    private Device fromDtoToEntity(DeviceDto deviceDto){
        Device device  = modelMapper.map(deviceDto,Device.class);
        if(deviceDto.getSensorUid()!=null)
            device.setSensor(sensorRepo.findByUid(deviceDto.getSensorUid()).orElseThrow(()-> new SensorNotFoundException(deviceDto.getSensorUid())));
        if(deviceDto.getOwnerUid()!=null)
            device.setOwner(userRepo.findByUid(deviceDto.getOwnerUid()).orElseThrow(()-> new UserNotFoundException(deviceDto.getOwnerUid())));
        return device;
    }

    private DeviceDto fromEntityToDto(Device device){
        DeviceDto deviceDto = modelMapper.map(device,DeviceDto.class);
        if(device.getOwner()!=null)
            deviceDto.setOwnerUid(device.getOwner().getUid());
        if(device.getSensor()!=null)
            deviceDto.setSensorUid(device.getSensor().getUid());
        return deviceDto;
    }
}
